
To build project:
```
$ sbt clean assembly
```

To run against sample data files:
```
$ cat src/test/resources/overlapping/from-the-problem-statement.txt | ./bounding-box
(1,1)(2,2)
```

There are other samples in 
```
src/
  test/
    resources/
      no-boxes.txt
      non-overlapping-groups.txt
      overlapping/
        from-the-problem-statement.txt	
        maximal.txt			
        minimal.txt
```
