package slacklife.challenges.axoni

import org.scalatest._
import wordspec.AnyWordSpec
import com.typesafe.scalalogging.LazyLogging
import org.scalatest.matchers.should.Matchers
import scala.util._
import scala.collection.mutable.Stack
import scala.math.Ordering
import BoundingBox._

class BoundingBoxSpec extends AnyWordSpec with Matchers with Inspectors with LazyLogging {

  "context" when {
    "should" should {
      "toGraphItems - simple" in {
        val sample =
          toGraphItems("""|----
               |-**-
               |-**-
               |----""".stripMargin)

        sample shouldBe
        Array(
          Array(
            Dash(Coordinate(1, 1)),
            Dash(Coordinate(2, 1)),
            Dash(Coordinate(3, 1)),
            Dash(Coordinate(4, 1))
          ),
          Array(
            Dash(Coordinate(1, 2)),
            Splat(Coordinate(2, 2)),
            Splat(Coordinate(3, 2)),
            Dash(Coordinate(4, 2))
          ),
          Array(
            Dash(Coordinate(1, 3)),
            Splat(Coordinate(2, 3)),
            Splat(Coordinate(3, 3)),
            Dash(Coordinate(4, 3))
          ),
          Array(
            Dash(Coordinate(1, 4)),
            Dash(Coordinate(2, 4)),
            Dash(Coordinate(3, 4)),
            Dash(Coordinate(4, 4))
          )
        )
      }

      "discoverEdge" in {
        val sample =
          toGraphItems(
            """|----
               |-**-
               |--*-
               |----""".stripMargin)

        discoverEdge(sample, Coordinate(2, 2)) shouldBe Set(
          Coordinate(2, 2),
          Coordinate(3, 2),
          Coordinate(3, 3)
        )
        discoverEdge(sample, Coordinate(3, 2)) shouldBe Set(
          Coordinate(2, 2),
          Coordinate(3, 2),
          Coordinate(3, 3)
        )
        discoverEdge(sample, Coordinate(3, 3)) shouldBe Set(
          Coordinate(2, 2),
          Coordinate(3, 2),
          Coordinate(3, 3)
        )

        discoverEdge(sample, Coordinate(3, 0)) shouldBe Set()
        discoverEdge(sample, Coordinate(0, 1)) shouldBe Set()
        discoverEdge(sample, Coordinate(2, 3)) shouldBe Set()
      }
    }

    "box boundaries" in {
      val sample =
        toGraphItems(
          """|--*-------
             |-**-------
             |--*******-
             |------*---""".stripMargin)

      upperLeft(discoverEdge(sample, Coordinate(2, 2))) shouldBe Some(Coordinate(2, 1))
      lowerRight(discoverEdge(sample, Coordinate(2, 2))) shouldBe Some(Coordinate(9, 4))
    }

    "edgeToBox" in {
      edgeToBox(
        Set(
          Coordinate(1, 1),
          Coordinate(2, 1),
          Coordinate(2, 2)
        )
      ) shouldBe Some(Box(Coordinate(1, 1), Coordinate(2, 2)))

      edgeToBox(Set.empty[Coordinate]) shouldBe None
    }

    "box overlaps" in {
      // overlaps itself
      Box(Coordinate(1, 1), Coordinate(1, 1))
        .overlaps(Box(Coordinate(1, 1), Coordinate(1, 1))) shouldBe true

      // non overlap, disjoint
      Box(Coordinate(1, 1), Coordinate(1, 1))
        .overlaps(Box(Coordinate(0, 0), Coordinate(0, 0))) shouldBe false

      // overlap top left
      Box(Coordinate(1, 1), Coordinate(3, 3))
        .overlaps(Box(Coordinate(0, 0), Coordinate(2, 2))) shouldBe true

      // overlap bottom right
      Box(Coordinate(1, 1), Coordinate(3, 3))
        .overlaps(Box(Coordinate(3, 3), Coordinate(5, 5))) shouldBe true

      // overlap top right
      Box(Coordinate(1, 1), Coordinate(3, 3))
        .overlaps(Box(Coordinate(3, 1), Coordinate(5, 5))) shouldBe true

      // overlap bottom left
      Box(Coordinate(1, 1), Coordinate(3, 3))
        .overlaps(Box(Coordinate(0, 3), Coordinate(1, 4))) shouldBe true

      // criss-cross
      Box(Coordinate(3, 0), Coordinate(3, 10))
        .overlaps(Box(Coordinate(0, 5), Coordinate(10, 5))) shouldBe true
    }

    "reject overlapping boxes" in {
      // non-overlapping
      markOverlappage(
        Box(Coordinate(0, 0), Coordinate(1, 1)),
        List()
      ) shouldBe Seq(
        Box(Coordinate(0, 0), Coordinate(1, 1))
      )

      // non-overlapping
      markOverlappage(
        Box(Coordinate(8, 0), Coordinate(11, 2)),
        List(Box(Coordinate(0, 0), Coordinate(1, 1)))
      ) shouldBe Seq(
        Box(Coordinate(0, 0), Coordinate(1, 1)),
        Box(Coordinate(8, 0), Coordinate(11, 2))
      )

      // overlappage
      markOverlappage(
        Box(Coordinate(4, 1), Coordinate(9, 3)),
        List(Box(Coordinate(0, 0), Coordinate(1, 1)), Box(Coordinate(8, 0), Coordinate(11, 2)))
      ) shouldBe Seq(
        Box(Coordinate(0, 0), Coordinate(1, 1)),
        Box(Coordinate(8, 0), Coordinate(11, 2), true),
        Box(Coordinate(4, 1), Coordinate(9, 3), true)
      )
    }

    "box area" in {
      Box(Coordinate(0, 0), Coordinate(0, 0)).area shouldBe 1
      Box(Coordinate(0, 0), Coordinate(0, 1)).area shouldBe 2
      Box(Coordinate(0, 0), Coordinate(1, 1)).area shouldBe 4
    }

    "max box area" in {
      List(
        Box(Coordinate(0, 0), Coordinate(0, 0)),
        Box(Coordinate(0, 0), Coordinate(0, 1)),
        Box(Coordinate(0, 0), Coordinate(1, 1))
      ).maxOption(MaxBoxArea) shouldBe Some(Box(Coordinate(0, 0), Coordinate(1, 1)))

      List().maxOption(MaxBoxArea) shouldBe None
    }
  }
}
