package slacklife.challenges.axoni

import org.scalatest._
import wordspec.AnyWordSpec
import com.typesafe.scalalogging.LazyLogging
import org.scalatest.matchers.should.Matchers
import BoundingBox._

class FindEdgesSpec extends AnyWordSpec with Matchers with Inspectors with LazyLogging {

  "find edges" when {
    "should" should {
      "coords in edge" in {
        Set(Coordinate(0, 0), Coordinate(0, 1), Coordinate(0, 2)).contains(Coordinate(0, 1))
      }
      "scenario 1" in {
        edgesFrom(
            """|*-*--
               |*--*-
               |*--*-
               |***--""".stripMargin).length shouldBe 3
      }
    }
  }
}
