package slacklife.challenges.axoni

import com.typesafe.scalalogging.LazyLogging
import scala.util._

object BoundingBox extends LazyLogging {

  type Edge = Set[Coordinate]

  val edgesFrom = findEdges _ compose toGraphItems _

  def findEdges(universe: Array[Array[GraphItem]]): List[Edge] = 
    universe.foldLeft(List.empty[Edge])((acc, row) => {
      row.foldLeft(acc)((edges, graphItem) => {
        graphItem match {
          case Splat(coords) if edges.forall(e => !e.contains(coords)) =>
            discoverEdge(universe, coords) :: edges
          case _ => edges
        }
      })
    })

  def markOverlappage(box: Box, boxes: List[Box]): List[Box] =
    boxes match { 
      case head :: tail if head.overlaps(box) =>
        head.copy(overlapping = true) :: markOverlappage(box.copy(overlapping = true), tail)
      case head :: tail => head :: markOverlappage(box, tail)
      case _            => List(box)
    }

  def edgeToBox(edge: Edge): Option[Box] =
    for {
      ul <- upperLeft(edge) 
      lr <- lowerRight(edge)
    } yield Box(ul, lr)

  def upperLeft(edge: Edge): Option[Coordinate] = {
    edge match {
      case _ if edge.isEmpty => Option.empty[Coordinate]
      case _                 => Some(Coordinate(edge.map(_.x).min, edge.map(_.y).min))
    }
  }

  def lowerRight(edge: Edge): Option[Coordinate] = {
    edge match {
      case _ if edge.isEmpty => Option.empty[Coordinate]
      case _                 => Some(Coordinate(edge.map(_.x).max, edge.map(_.y).max))
    }
  }

  def toGraphItems(input: String): Array[Array[GraphItem]] = {
    val initArray = Array.empty[Array[GraphItem]]
    input
      .foldLeft(((1, 1), initArray.:+(Array.empty[GraphItem])))((acc, item) => {
        item match {
          case '*' =>
            acc._2(acc._1._2 - 1) = acc._2(acc._1._2 - 1).appended(Splat(Coordinate(acc._1._1, acc._1._2)))
            ((acc._1._1 + 1, acc._1._2), acc._2)
          case '-' =>
            acc._2(acc._1._2 - 1) = acc._2(acc._1._2 - 1).appended(Dash(Coordinate(acc._1._1, acc._1._2)))
            ((acc._1._1 + 1, acc._1._2), acc._2)
          case _ => {
            // new row, add array
            ((1, acc._1._2 + 1), acc._2.:+(Array.empty[GraphItem]))
          }
        }
      })
      ._2
  }

  def discoverEdge(
    universe: Array[Array[GraphItem]],
    coord: Coordinate,
    acc: Edge = Set.empty[Coordinate]
  ): Edge = {
    Try(universe(coord.y - 1)(coord.x - 1)) match {
      case Success(Splat(_)) if (!acc.contains(coord)) =>
        val edge =
          discoverEdge(universe, coord.left, acc + coord) ++
          discoverEdge(universe, coord.right, acc + coord) ++
          discoverEdge(universe, coord.up, acc + coord) ++
          discoverEdge(universe, coord.down, acc + coord)

        edge + coord
      case x => Set.empty[Coordinate]
    }
  }

}
