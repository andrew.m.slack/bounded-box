package slacklife.challenges.axoni

import BoundingBox._
import com.typesafe.scalalogging.LazyLogging

object App extends LazyLogging {

  def main(args: Array[String]): Unit =
    System.exit(
      edgesFrom(
        io.Source.stdin.getLines
          .map(line => line ++ System.lineSeparator())
          .reduce(_ + _)
      ).flatMap(edgeToBox)
       .foldLeft(List.empty[Box])((acc, box) => markOverlappage(box, acc))
       .filterNot(_.overlapping)
       .maxOption(MaxBoxArea)
       .map(box => {
          System.out.println(s"${box}")
          0
       }).getOrElse(1)
    )
}
