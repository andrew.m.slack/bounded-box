package slacklife.challenges.axoni

case class Coordinate(x: Int, y: Int) {
  def left = this.copy(x = x - 1)
  def right = this.copy(x = x + 1)
  def up = this.copy(y = y - 1)
  def down = this.copy(y = y + 1)
  override def toString = s"($x,$y)"
}

case class Box(upperLeft: Coordinate, bottomRight: Coordinate, overlapping: Boolean = false) {
  override def toString = s"$upperLeft$bottomRight"
  def horizontalRange = upperLeft.x to bottomRight.x
  def verticalRange = upperLeft.y to bottomRight.y

  def overlaps(other: Box): Boolean =
    (other.horizontalRange intersect this.horizontalRange).nonEmpty && (other.verticalRange intersect this.verticalRange).nonEmpty

  def contains(coordinate: Coordinate): Boolean =
    (upperLeft.x <= coordinate.x) && (coordinate.x <= bottomRight.x) && (upperLeft.y <= coordinate.y) && (coordinate.y <= bottomRight.y)

  def area = horizontalRange.length * verticalRange.length
}

object MaxBoxArea extends Ordering[Box] {
  override def compare(x: Box, y: Box): Int = x.area compareTo y.area
}

trait GraphItem
case class Splat(coordinate: Coordinate) extends GraphItem
case class Dash(coordinate: Coordinate) extends GraphItem
