
lazy val root = (project in file("."))
  .enablePlugins(JmhPlugin)
  .settings(
    name := "bounding-box",
    organization := "slack.life",
    description := "axoni problem",
    licenses += "Apache License, Version 2.0" -> url(
      "https://www.apache.org/licenses/LICENSE-2.0"
    ),
    version := "0.1",
    scalaVersion := "2.13.1",
    assemblyJarName in assembly := "axoni-scala.jar",
    mainClass in (assembly) := Some("slacklife.challenges.axoni.App"),
    ThisBuild / useSuperShell := false,
    bloopExportJarClassifiers in Global := Some(Set("sources")),
    libraryDependencies ++= Seq(
      "org.typelevel" %% "cats-core" % "2.0.0",
      "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
      "ch.qos.logback" % "logback-classic" % "1.2.3",
      "org.scalactic" %% "scalactic" % "3.1.1" % Test,
      "org.scalatest" %% "scalatest" % "3.1.1" % Test,
      "org.scalacheck" %% "scalacheck" % "1.14.1" % Test,
      "org.scalamock" %% "scalamock" % "5.0.0" % Test
    ))

